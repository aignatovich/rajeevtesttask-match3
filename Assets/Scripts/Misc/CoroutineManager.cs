﻿using System.Collections;
using UnityEngine;

// ToDo: Rename some shit
public class CoroutineManager : MonoBehaviour
{
    public bool IsEmpty
    {
        get { return count == 0; }
    }

    int count = 0;

    public void RunCourutine(IEnumerator enumerator)
    {
        count++;
        StartCoroutine(DoCourutine(enumerator));
    }

    IEnumerator DoCourutine(IEnumerator enumerator)
    {
        while (enumerator.MoveNext())
        {
            yield return enumerator.Current;
        }

        count--;
    }
}
