﻿using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static int GetTileColumn(int index, int gridWidth, int gridHeight)
    {
        // ToDo: check width and height
        return index % gridWidth;
    }

    public static int GetTileRow(int index, int gridWidth, int gridHeight)
    {
        // ToDo: check width and height
        return index / gridWidth;
    }

    public static int GetTileIndex(int column, int row, int gridWidth)
    {
        // ToDo: check width and height
        return row * gridWidth + column;
    }

    public static bool IsNeighbours(int indexA, int indexB, int gridWidth)
    {
        int distance = Mathf.Abs(indexA - indexB);
        return distance == gridWidth || distance == 1;
    }

    public static bool IsValidIndex(int indexA, int gridWidth, int gridHeight)
    {
        return (indexA >= 0) && (indexA < gridWidth * gridHeight);
    }

    public static int RandomExcept(int min, int max, List<int> except)
    {
        int value = -1;
        do
        {
            value = Random.Range(min, max);
        }
        while (except.Contains(value));

        return value;
    }
}
