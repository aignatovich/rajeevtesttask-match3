﻿using UnityEngine;

[CreateAssetMenu(fileName = "New LevelSettings", menuName = "LevelSettings", order = 51)]
public class LevelSettings : ScriptableObject
{
    [SerializeField] int gridWidth = 0;
    [SerializeField] int gridHeight = 0;
    [SerializeField] int colorsCount = 0;

    public int GridWidth
    {
        get { return gridWidth; }
    }

    public int GridHeight
    {
        get { return gridHeight; }
    }

    public int ColorsCount
    {
        get { return colorsCount; }
    }
}
