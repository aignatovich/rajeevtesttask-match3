﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//ToDo: to serialize field
[CreateAssetMenu(fileName = "New ApplicationSettings", menuName = "ApplicationSettings", order = 53)]
public class ApplicationSettings : ScriptableObject
{
    public const string PathToTilePrefab = "Prefabs/Tile";
    public const float TileFallSpeed = 4;

    public readonly Color[] Colors = new Color[]
    {
        Color.red,
        Color.blue,
        Color.green,
        Color.yellow,
        Color.white,
        Color.black,
    };


    public Color GetTileColor(int index)
    {
        if (index < 0 || index >= Colors.Length)
        {
            Debug.LogError(string.Format("Wrong color index:{0} colors count: {1}", index, Colors.Length));

            return Colors[Random.Range(0, Colors.Length)];
        }

        return Colors[index];
    }
}
