﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Simulator
{
    const int TargetMatch = 3;
    const int PotentialMatch = 2;

    public event Action SimulationFinished;

    public int Turn { get; private set; }

    LevelSettings levelSettings;
    int[,] tiles;
    int gridHeight;
    int gridWidth;

    public Simulator(LevelSettings levelSettings)
    {
        this.levelSettings = levelSettings;
        gridHeight = levelSettings.GridHeight;
        gridWidth = levelSettings.GridWidth;
        tiles = new int[gridWidth, gridHeight];

        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                tiles[i, j] = Constants.InvalidValue;
            }
        }

        Turn = -1;
    }

    // Cach for optimisation
    List<SimulationAction> simulationResult = new List<SimulationAction>();
    List<SimulationAction> matchResult = new List<SimulationAction>();
    public SimulationResult Simulate(int indexA = 0, int indexB = 1)
    {
        Turn++;
        if (Turn == 0)
        {
            var firstSchema = new SimulationResult(Turn, indexA, indexB, TryCreateTiles().ToArray());
            return firstSchema;
        }

        simulationResult.Clear();
        if (!Utils.IsNeighbours(indexA, indexB, gridWidth) ||
            !Utils.IsValidIndex(indexA, gridWidth, gridHeight) ||
            !Utils.IsValidIndex(indexB, gridWidth, gridHeight))
        {
            simulationResult.Add(new SimulationAction(ActionType.Error, indexA, Constants.InvalidValue, indexB));
            return new SimulationResult(Turn, indexA, indexB, simulationResult.ToArray());
        }

        var swapResult = TrySwap(indexA, indexB);
        simulationResult.AddRange(swapResult);

        List<int> matchTiles = new List<int>();
        for (int i = 0; i < swapResult.Count; i++)
        {
            matchTiles.Add(swapResult[i].Position);
        }

        matchResult.Clear();
        do
        {
            matchResult = TryMatchtTiles(matchTiles);
            if (matchResult != null)
            {
                matchTiles.Clear();

                simulationResult.AddRange(matchResult);

                var fallen = TryFallTiles();
                simulationResult.AddRange(fallen);

                for (int i = 0; i < fallen.Length; i++)
                {
                    matchTiles.Add(fallen[i].EndPosition);
                }
            }
        }
        while (matchResult.Count != 0);

        var created = TryCreateTiles();
        simulationResult.AddRange(created);

        for (int i = 0; i < created.Count; i++)
        {
            matchTiles.Add(created[i].Position);
        }

        SimulationFinished?.Invoke();

        return new SimulationResult(Turn, indexA, indexB, simulationResult.ToArray());
    }

    List<SimulationAction> TryCreateTiles()
    {
        List<SimulationAction> result = new List<SimulationAction>();
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                if (tiles[i, j] == Constants.InvalidValue)
                {
                    tiles[i, j] = GetNewCellType(i, j);

                    int index = Utils.GetTileIndex(i, j, gridWidth);
                    result.Add(new SimulationAction(ActionType.Create, index, tiles[i, j]));
                }
            }
        }

        return result;
    }

    List<SimulationAction> TrySwap(int indexA, int indexB)
    {
        List<SimulationAction> result = new List<SimulationAction>();
        if (!Utils.IsValidIndex(indexA, gridWidth, gridHeight))
        {
            Debug.LogErrorFormat("invalid indexA:{0}", indexA);
            return result;
        }

        if (!Utils.IsValidIndex(indexB, gridWidth, gridHeight))
        {
            Debug.LogErrorFormat("invalid indexB:{0}", indexB);
            return result;
        }

        if (!Utils.IsNeighbours(indexA, indexB, gridWidth))
        {
            Debug.LogErrorFormat("Wrong indexies for swap:{0} and {1}", indexA, indexB);
            return result;
        }

        int columnA = Utils.GetTileColumn(indexA, gridWidth, gridHeight);
        int rowA = Utils.GetTileRow(indexA, gridWidth, gridHeight);
        int typeA = tiles[columnA, rowA];

        int columnB = Utils.GetTileColumn(indexB, gridWidth, gridHeight);
        int rowB = Utils.GetTileRow(indexB, gridWidth, gridHeight);
        int typeB = tiles[columnB, rowB];

        tiles[columnA, rowA] = typeB;
        tiles[columnB, rowB] = typeA;

        result.Add(new SimulationAction(ActionType.SuccessSwap, indexA, Constants.InvalidValue, indexB));
        result.Add(new SimulationAction(ActionType.SuccessSwap, indexB, Constants.InvalidValue, indexA));

        return result;
    }

    // Cach for optimisation
    List<SimulationAction> tryMatchResult = new List<SimulationAction>();
    List<SimulationAction> TryMatchtTiles(List<int> checkTiles)
    {
        if (checkTiles == null)
        {
            return null;
        }

        tryMatchResult.Clear();
        for (int i = 0; i < checkTiles.Count; i++)
        {
            var matched = CheckMatch(checkTiles[i]);
            if (matched != null)
            {
                for (int j = 0; j < matched.Count; j++)
                {
                    tryMatchResult.Add(new SimulationAction(ActionType.Destroy, matched[j]));

                    int column = Utils.GetTileColumn(matched[j], gridWidth, gridHeight);
                    int row = Utils.GetTileRow(matched[j], gridWidth, gridHeight);
                    tiles[column, row] = Constants.InvalidValue;
                }
            }
        }

        return tryMatchResult;
    }

    // Cach for optimisation
    List<SimulationAction> fallResult = new List<SimulationAction>();
    SimulationAction[] TryFallTiles()
    {
        fallResult.Clear();
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                if (tiles[i, j] == Constants.InvalidValue)
                {
                    int shift = 0;
                    for (int k = j; k < gridHeight; k++)
                    {
                        if (tiles[i, k] != Constants.InvalidValue)
                        {
                            break;
                        }

                        shift++;
                    }

                    for (int row = j + shift; row < gridHeight; row++)
                    {
                        if (tiles[i, row] == Constants.InvalidValue)
                        {
                            //Debug.LogError("InvalidValue in " + Turn);
                            continue;
                        }

                        tiles[i, row - shift] = tiles[i, row];
                        tiles[i, row] = Constants.InvalidValue;

                        int position = Utils.GetTileIndex(i, row, gridWidth);
                        int endPosition = Utils.GetTileIndex(i, row - shift, gridWidth);
                        fallResult.Add(new SimulationAction(ActionType.Fall, position, Constants.InvalidValue, endPosition));
                    }
                }
            }
        }

        return fallResult.ToArray();
    }

    // Cach for optimisation
    List<int> resultCheck = new List<int>();
    List<int> matchesCheck = new List<int>();
    //ToDo: Copypast
    List<int> CheckMatch(int position)
    {
        resultCheck.Clear();
        matchesCheck.Clear();

        int originalColumn = Utils.GetTileColumn(position, gridWidth, gridHeight);
        int originalRow = Utils.GetTileRow(position, gridWidth, gridHeight);
        int type = tiles[originalColumn, originalRow];

        int column = Mathf.Clamp(originalColumn - TargetMatch, 0, gridWidth);
        int maxColumn = Mathf.Clamp(originalColumn + TargetMatch, 0, gridWidth);

        while (column < maxColumn)
        {
            if (tiles[column, originalRow] == type)
            {
                matchesCheck.Add(Utils.GetTileIndex(column, originalRow, gridWidth));
            }
            else
            {
                if (matchesCheck.Count >= TargetMatch)
                {
                    resultCheck.AddRange(matchesCheck);
                }

                matchesCheck.Clear();
            }

            column++;
        }

        if (matchesCheck.Count >= TargetMatch)
        {
            resultCheck.AddRange(matchesCheck);
        }

        matchesCheck.Clear();

        int row = Mathf.Clamp(originalRow - TargetMatch, 0, gridHeight);
        int maxRow = Mathf.Clamp(originalRow + TargetMatch, 0, gridHeight);

        while (row < maxRow)
        {
            if (tiles[originalColumn, row] == type)
            {
                int matchedIndex = Utils.GetTileIndex(originalColumn, row, gridWidth);
                if (!matchesCheck.Contains(matchedIndex))
                {
                    matchesCheck.Add(matchedIndex);
                }
            }
            else
            {
                if (matchesCheck.Count >= TargetMatch)
                {
                    resultCheck.AddRange(matchesCheck);
                }

                matchesCheck.Clear();
            }

            row++;
        }

        if (matchesCheck.Count >= TargetMatch)
        {
            resultCheck.AddRange(matchesCheck);
        }

        if (resultCheck.Count >= TargetMatch)
        {
            return resultCheck;
        }

        return null;
    }

    //ToDo: remove coopypast
    // Cach for optimisation
    List<int> wrongType = new List<int>();
    int GetNewCellType(int originalColumn, int originalRow)
    {
        wrongType.Clear();
        int prevColumn = Mathf.Clamp(originalColumn - PotentialMatch, 0, gridWidth);
        int maxColumn = Mathf.Clamp(originalColumn + PotentialMatch, 0, gridWidth);
        int prevType = tiles[prevColumn, originalRow];

        for (int i = prevColumn + 1; i < maxColumn; i++)
        {
            if (i == originalColumn)
                continue;

            if (prevType == tiles[i, originalRow])
            {
                wrongType.Add(prevType);
            }

            prevType = tiles[i, originalRow];
        }

        int prevRow = Mathf.Clamp(originalRow - PotentialMatch, 0, gridHeight);
        int maxRow = Mathf.Clamp(originalRow + PotentialMatch, 0, gridHeight);
        prevType = tiles[originalColumn, prevRow];
        for (int i = prevRow + 1; i < maxRow; i++)
        {
            if (i == originalRow)
                continue;

            if (prevType == tiles[originalColumn, i])
            {
                wrongType.Add(prevType);
            }

            prevType = tiles[originalColumn, i];
        }

        return Utils.RandomExcept(0, levelSettings.ColorsCount, wrongType);
    }
}
