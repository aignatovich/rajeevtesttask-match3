﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{
    public event Action<int> SimulateTurnsClicked;
    public event Action NextTurnClicked;
    public event Action PrevTurnsClicked;
    public event Action PlayAllClicked;

    [SerializeField]
    Button SimulateButton = null;
    [SerializeField]
    Button NextTurn = null;
    [SerializeField]
    Button PrevTurn = null;
    [SerializeField]
    Button PlayAll = null;

    [SerializeField]
    Text Turns = null;
    [SerializeField]
    InputField InputSimulation = null;

    void Awake()
    {
        SimulateButton.onClick.AddListener(OnClickSimulate);
        NextTurn.onClick.AddListener(OnClickNextTurn);
        PrevTurn.onClick.AddListener(OnClickPrevTurn);
        PlayAll.onClick.AddListener(OnClickPlayAll);
    }

    void OnClickPlayAll()
    {
        PlayAllClicked?.Invoke();
    }

    void OnClickPrevTurn()
    {
        PrevTurnsClicked?.Invoke();
    }

    void OnClickNextTurn()
    {
        NextTurnClicked?.Invoke();
    }

    void OnClickSimulate()
    {
        int turns = 0;
        if (int.TryParse(InputSimulation.text, out turns))
        {
            if (turns > 0)
            {
                SimulateTurnsClicked?.Invoke(turns);
            }
        }
    }

    public void UpdateTurns(int cur, int max)
    {
        Turns.text = string.Format("turn:{0} max{1}", cur, max);
    }
}
