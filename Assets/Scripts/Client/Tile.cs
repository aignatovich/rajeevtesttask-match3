﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public int Position { get; private set; }
    public int Type { get; private set; }

    [SerializeField]
    SpriteRenderer sprite = null;

    public void Init(Color color, int position, int type)
    {
        sprite.color = color;
        Position = position;
        Type = type;
    }

    public void UpdateIndex(int position)
    {
        Position = position;
    }
}
