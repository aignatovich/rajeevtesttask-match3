﻿using System;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public event Action<int> TileTapped;
    public event Action ForwardPressed;
    public event Action BackwardPressed;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.transform != null)
            {
                var tile = hit.transform.gameObject.GetComponent<Tile>();
                if (tile != null)
                {
                    TileTapped(tile.Position);
                }
            }
        }

        if(Input.GetButtonDown("Forward"))
        {
            ForwardPressed?.Invoke();
        }

        if (Input.GetButtonDown("Backward"))
        {
            BackwardPressed?.Invoke();
        }
    }   
}
