﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesPlayer : MonoBehaviour
{
    public int Turn { get; private set; }

    public bool IsPlaying { get; private set; }
    public event Action FinishPlaying;

    LevelSettings levelSettings;
    ApplicationSettings applicationSettings;
    GameObjectsPool pool;
    CoroutineManager coroutineManager;

    Tile[,] tiles;

    public void Init(LevelSettings levelSettings, ApplicationSettings applicationSettings, GameObjectsPool pool)
    {
        this.levelSettings = levelSettings;
        this.applicationSettings = applicationSettings;
        this.pool = pool;

        Turn = -1;
        coroutineManager = gameObject.AddComponent<CoroutineManager>();
        tiles = new Tile[levelSettings.GridWidth, levelSettings.GridHeight];
    }

    public void SetupWorld(int[,] world, int turn)
    {
        Turn = turn;

        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                if (tiles[i, j] != null)
                {
                    pool.Despawn(ApplicationSettings.PathToTilePrefab, tiles[i, j].gameObject);
                }

                // ToDo: copypast
                int index = Utils.GetTileIndex(i, j, levelSettings.GridWidth);
                var tile = pool.Spawn<Tile>(ApplicationSettings.PathToTilePrefab, Vector3.zero, 0);
                tile.Init(applicationSettings.GetTileColor(world[i, j]), index, world[i, j]);
                tile.transform.position = GetTilePosition(index);
                tile.transform.parent = transform;
                tiles[i, j] = tile;
            }
        }
    }

    public int[,] GetWorldSlice()
    {
        int[,] world = new int[tiles.GetLength(0), tiles.GetLength(1)];
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                if (tiles[i, j] != null)
                    world[i, j] = tiles[i, j].Type;
            }
        }

        return world;
    }

    public void Play(SimulationResult simulationResult)
    {
        if (simulationResult.Step - Turn != 1)
        {
            Debug.LogErrorFormat("cant play step:{0} after {1}", simulationResult.Step, Turn);
            return;
        }

        Turn++;
        IsPlaying = true;

        StartCoroutine(DoPlay(simulationResult.Actions));
    }

    IEnumerator DoPlay(SimulationAction[] simulationResult)
    {
        List<Vector2> spawnPositions = new List<Vector2>();
        int index = 0;

        while (index < simulationResult.Length)
        {
            ActionType actionType = simulationResult[index].Action;
            while (index < simulationResult.Length && simulationResult[index].Action == actionType)
            {
                switch (simulationResult[index].Action)
                {
                    case ActionType.Create:
                        {
                            coroutineManager.RunCourutine(Create(simulationResult[index].Position, simulationResult[index].Type, spawnPositions));
                        }
                        break;
                    case ActionType.SuccessSwap:
                        {
                            coroutineManager.RunCourutine(Swap(simulationResult[index].Position, simulationResult[index].EndPosition));
                        }
                        break;
                    case ActionType.Destroy:
                        {
                            coroutineManager.RunCourutine(Destroy(simulationResult[index].Position));
                        }
                        break;
                    case ActionType.Fall:
                        {
                            coroutineManager.RunCourutine(Fall(simulationResult[index].Position, simulationResult[index].EndPosition));
                        }
                        break;
                    case ActionType.Error:
                        {
                            Debug.LogError("Simulation error");
                        }
                        break;
                }

                index++;
            }

            yield return new WaitUntil(() => coroutineManager.IsEmpty);
        }

        yield return new WaitForSeconds(Constants.PauseAfterTurn);

        IsPlaying = false;
        FinishPlaying?.Invoke();
    }

    public IEnumerator Create(int index, int type, List<Vector2> spawnPositions)
    {
        var tile = pool.Spawn<Tile>(ApplicationSettings.PathToTilePrefab, Vector3.zero, 0);
        tile.Init(applicationSettings.GetTileColor(type), index, type);
        tile.transform.position = GetTileStartPosition(index, spawnPositions);
        tile.transform.parent = transform;

        int column = Utils.GetTileColumn(index, levelSettings.GridWidth, levelSettings.GridHeight);
        int row = Utils.GetTileRow(index, levelSettings.GridWidth, levelSettings.GridHeight);
        tiles[column, row] = tile;

        Vector2 position = GetTilePosition(index);
        while (tile.transform.position.y > position.y)
        {
            tile.transform.position += Vector3.down * Time.deltaTime * ApplicationSettings.TileFallSpeed;
            yield return null;
        }

        tile.transform.position = position;
    }

    public IEnumerator Fall(int indexA, int indexB)
    {
        Tile tile = GetTile(indexA);
        if (tile == null)
        {
            Debug.LogErrorFormat("Cant move Tile from {0} to {1}", indexA, indexB);
            yield break;
        }

        Vector3 positionA = GetTilePosition(indexA);
        Vector3 positionB = GetTilePosition(indexB);

        while (Vector3.Distance(tile.transform.position, positionB) > Time.deltaTime)
        {
            tile.transform.position += (positionB - tile.transform.position) * Time.deltaTime * ApplicationSettings.TileFallSpeed;
            yield return null;
        }

        tile.transform.position = positionB;

        int columnA = Utils.GetTileColumn(indexA, levelSettings.GridWidth, levelSettings.GridHeight);
        int rowA = Utils.GetTileRow(indexA, levelSettings.GridWidth, levelSettings.GridHeight);

        int columnB = Utils.GetTileColumn(indexB, levelSettings.GridWidth, levelSettings.GridHeight);
        int rowB = Utils.GetTileRow(indexB, levelSettings.GridWidth, levelSettings.GridHeight);

        if (tiles[columnA, rowA] == tile)
        {
            tiles[columnA, rowA] = null;
        }

        tiles[columnB, rowB] = tile;
        tile.UpdateIndex(indexB);
    }

    // ToDo: Mess
    public IEnumerator Swap(int indexA, int indexB)
    {
        Tile tileA = GetTile(indexA);
        Tile tileB = GetTile(indexB);

        Vector3 positionA = GetTilePosition(indexB);
        Vector3 positionB = GetTilePosition(indexA);

        while (Vector3.Distance(tileA.transform.position, positionA) > Time.deltaTime)
        {
            tileA.transform.position += (positionA - tileA.transform.position) * Time.deltaTime * ApplicationSettings.TileFallSpeed;
            tileB.transform.position += (positionB - tileB.transform.position) * Time.deltaTime * ApplicationSettings.TileFallSpeed;
            yield return null;
        }

        tileA.transform.position = positionA;
        tileB.transform.position = positionB;

        int columnA = Utils.GetTileColumn(indexA, levelSettings.GridWidth, levelSettings.GridHeight);
        int rowA = Utils.GetTileRow(indexA, levelSettings.GridWidth, levelSettings.GridHeight);

        int columnB = Utils.GetTileColumn(indexB, levelSettings.GridWidth, levelSettings.GridHeight);
        int rowB = Utils.GetTileRow(indexB, levelSettings.GridWidth, levelSettings.GridHeight);

        tiles[columnA, rowA] = tileB;
        tiles[columnB, rowB] = tileA;

        tileB.UpdateIndex(indexA);
        tileA.UpdateIndex(indexB);
    }

    IEnumerator Destroy(int indexA)
    {
        int columnA = Utils.GetTileColumn(indexA, levelSettings.GridWidth, levelSettings.GridHeight);
        int rowA = Utils.GetTileRow(indexA, levelSettings.GridWidth, levelSettings.GridHeight);

        if (tiles[columnA, rowA] == null)
        {
            //tile allready destoyed 
            yield break;
        }

        pool.Despawn(ApplicationSettings.PathToTilePrefab, tiles[columnA, rowA].gameObject);
        tiles[columnA, rowA] = null;

        yield return null;
    }

    Tile GetTile(int index)
    {
        int column = Utils.GetTileColumn(index, levelSettings.GridWidth, levelSettings.GridHeight);
        int row = Utils.GetTileRow(index, levelSettings.GridWidth, levelSettings.GridHeight);

        return tiles[column, row];
    }

    Vector2 GetTilePosition(int index)
    {
        float halfWidth = levelSettings.GridWidth / 2;
        float halfHeight = levelSettings.GridHeight / 2;

        int column = Utils.GetTileColumn(index, levelSettings.GridWidth, levelSettings.GridHeight);
        int row = Utils.GetTileRow(index, levelSettings.GridWidth, levelSettings.GridHeight);

        return new Vector2(column - halfWidth, row - halfHeight);
    }

    Vector2 GetTileStartPosition(int index, List<Vector2> spawnPositions)
    {
        float halfHeight = levelSettings.GridHeight / 2;
        Vector2 position = GetTilePosition(index);
        position.y = halfHeight + 1;

        if (spawnPositions.Count != 0)
        {
            var positionIndex = 0;
            while (positionIndex != Constants.InvalidValue)
            {
                positionIndex = spawnPositions.FindIndex(s => Vector2.Distance(s, position) < Mathf.Epsilon);
                if (positionIndex != Constants.InvalidValue)
                {
                    position.y += 1;
                }
            }
        }

        spawnPositions.Add(position);
        return position;
    }
}
