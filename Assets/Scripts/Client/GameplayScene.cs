﻿using System.Collections;
using UnityEngine;

//ToDo: rename to Game or core, root
public class GameplayScene : MonoBehaviour
{
    enum State
    {
        WaitingInput,
        Playback,// use it more carefully remove(turn+1)
        WaitingSimulationResult,
        Playing,
    }

    [SerializeField]
    LevelSettings levelSettings = null;
    [SerializeField]
    ApplicationSettings applicationSettings = null;

    #region Server
    Simulator simulator;
    Ai ai;
    #endregion

    #region Client
    TilesPlayer cellsManager;
    PlayerInput userInput;
    Hud hud;
    History history;
    #endregion

    GameObjectsPool pool;
    State state;
    int firstTile;

    void Start()
    {
        state = State.Playing;
        firstTile = Constants.InvalidValue;

        pool = new GameObjectsPool();

        IdGenerator idGenerator = new IdGenerator();
        history = new History();

        userInput = new GameObject("UserInput").AddComponent<PlayerInput>();
        userInput.TileTapped += OnTileTapped;
        userInput.ForwardPressed += OnWorldForward;
        userInput.BackwardPressed += OnWorldBackward;

        cellsManager = new GameObject("CellsManager").AddComponent<TilesPlayer>();
        cellsManager.Init(levelSettings, applicationSettings, pool);
        cellsManager.FinishPlaying += OnFinishPlaying;

        hud = FindObjectOfType<Hud>();
        hud.SimulateTurnsClicked += OnSimulateTurnsClicked;
        hud.NextTurnClicked += OnNextTurnClicked;
        hud.PrevTurnsClicked += OnPrevTurnsClicked;
        hud.PlayAllClicked += OnPlayAllClicked;

        ai = new Ai(levelSettings.GridWidth, levelSettings.GridHeight);
        simulator = new Simulator(levelSettings);

        var simulationResult = simulator.Simulate();
        history.AddSimulationResult(simulationResult);

        //ToDo: Crete method for start simulation, use create world
        cellsManager.Play(simulationResult);
        state = State.Playing;

        hud.UpdateTurns(cellsManager.Turn, simulator.Turn);
    }

    #region Callbacks from Hud
    void OnPlayAllClicked()
    {
        if (state == State.WaitingInput)
        {
            StartCoroutine(DoPlayAll());
        }
    }

    IEnumerator DoPlayAll()
    {
        state = State.Playback;
        while (cellsManager.Turn != simulator.Turn)
        {
            OnWorldForward();
            yield return null;// ToDo: Wait untile
        }

        state = State.WaitingInput;
        Debug.Log("Finish Play all");
    }

    void OnPrevTurnsClicked()
    {
        OnWorldBackward();
    }

    void OnNextTurnClicked()
    {
        OnWorldForward();
    }

    void OnSimulateTurnsClicked(int count)
    {
        for (int i = 0; i < count; i++)
        {
            int[] aiAction = ai.GetAction();
            var simulationResult = simulator.Simulate(aiAction[0], aiAction[1]);
            history.AddSimulationResult(simulationResult);
        }

        hud.UpdateTurns(cellsManager.Turn, simulator.Turn);
    }

    void OnTileTapped(int pos)
    {
        if (cellsManager.Turn != simulator.Turn)
            return;

        if (state == State.WaitingInput)
        {
            if (firstTile == Constants.InvalidValue)
            {
                firstTile = pos;
                return;
            }

            if (!Utils.IsNeighbours(firstTile, pos, levelSettings.GridWidth))
            {
                firstTile = Constants.InvalidValue;
                return;
            }

            var simulationResult = simulator.Simulate(firstTile, pos);
            history.AddSimulationResult(simulationResult);
            hud.UpdateTurns(cellsManager.Turn, simulator.Turn);

            firstTile = Constants.InvalidValue;
            cellsManager.Play(simulationResult);
            state = State.Playing;
        }
    }
    #endregion

    void OnWorldBackward()
    {
        if ((state == State.WaitingInput || state == State.Playback) && cellsManager.Turn > 0)
        {
            cellsManager.SetupWorld(history.GetWorldByTurn(cellsManager.Turn - 1), cellsManager.Turn - 1);
        }
    }

    void OnWorldForward()
    {
        if (state == State.WaitingInput || state == State.Playback)
        {
            var step = Mathf.Clamp(cellsManager.Turn + 1, 0, simulator.Turn);
            cellsManager.Play(history.GetSimulationTurn(step));
            if (cellsManager.IsPlaying)
            {
                state = State.Playing;
            }
        }
    }

    void OnFinishPlaying()
    {
        hud.UpdateTurns(cellsManager.Turn, simulator.Turn);
        history.AddWorldSlice(cellsManager.GetWorldSlice(), cellsManager.Turn);

        if (state == State.Playing)
        {
            if (cellsManager.Turn == simulator.Turn)
            {
                state = State.WaitingInput;
            }
            else
            {
                state = State.Playback;
            }
        }
    }
}
