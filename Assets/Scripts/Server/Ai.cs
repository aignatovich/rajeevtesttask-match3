﻿using UnityEngine;

public class Ai
{
    int width;
    int height;

    public Ai(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public int[] GetAction()
    {
        int firstColumn = Random.Range(0, width);
        int firstRow = Random.Range(0, height);

        int secondColumn = firstColumn;
        int secondRow = firstRow;

        int direction = Random.Range(0, 4);

        switch (direction)
        {
            //left
            case 0:
                secondColumn = secondColumn == 0 ? ++secondColumn : --secondColumn;
                break;
            //right
            case 1:
                secondColumn = secondColumn == (width - 1) ? --secondColumn : ++secondColumn;
                break;
            //up
            case 2:
                secondRow = secondRow == (height - 1) ? --secondRow : ++secondRow;
                break;
            //down
            case 3:
                secondRow = secondRow == 0 ? ++secondRow : --secondRow;
                break;
        }

        int firstIndex = Utils.GetTileIndex(firstColumn, firstRow, width);
        int secondIndex = Utils.GetTileIndex(secondColumn, secondRow, width);

        return new int[] { firstIndex, secondIndex };
    }
}
