﻿public enum ActionType
{
    Create,
    SuccessSwap,
    Destroy,
    Fall,
    Error
}

// Todo: split to different classes
public struct SimulationAction
{
    public ActionType Action;
    public int Position;
    public int Type;
    public int EndPosition;

    public SimulationAction(ActionType action, int position, int type = Constants.InvalidValue, int endPosition = Constants.InvalidValue)
    {
        Action = action;
        Type = type;
        Position = position;
        EndPosition = endPosition;
    }
}

public struct SimulationResult
{
    public int Step;
    public int PositionA;
    public int PositionB;
    public SimulationAction[] Actions;

    public SimulationResult(int step, int positionA, int positionB, SimulationAction[] actions)
    {
        Step = step;
        PositionA = positionA;
        PositionB = positionB;
        Actions = actions;
    }
}
