﻿using UnityEngine;
using System.Collections.Generic;

public class History
{
    List<SimulationResult> simulationsHistory;
    List<int[,]> worldsHistory;

    public History()
    {
        simulationsHistory = new List<SimulationResult>();
        worldsHistory = new List<int[,]>();
    }

    public void AddWorldSlice(int[,] world, int turn)
    {
        if (worldsHistory.Count == turn)
            worldsHistory.Add(world);
    }

    public void AddSimulationResult(SimulationResult simulations)
    {
        simulationsHistory.Add(simulations);
    }

    //ToDo: Refactor to Try method
    public int[,] GetWorldByTurn(int turn)
    {
        //step--;
        if (worldsHistory.Count == 0)
        {
            return null;
        }

        if (turn < 0)
        {
            return worldsHistory[0];
        }

        if (turn >= worldsHistory.Count)
        {
            return worldsHistory[worldsHistory.Count - 1];
        }

        return worldsHistory[turn];
    }

    //ToDo: Refactor to Try method
    public SimulationResult GetSimulationTurn(int step)
    {
        if (simulationsHistory.Count == 0)
        {
            Debug.LogError("ToDo");
            return new SimulationResult();
        }

        if (step < 0)
        {
            Debug.LogError("ToDo");
            return simulationsHistory[0];
        }

        if (step >= simulationsHistory.Count)
        {
            Debug.LogError("ToDo");
            return simulationsHistory[simulationsHistory.Count - 1];
        }

        return simulationsHistory[step];
    }
}
