﻿public static class Constants
{
    public const int InvalidValue = -1;
    public const float PauseAfterTurn = 0.1f;
}
